package com.leaves.auth.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时06分23秒
 * @Version 1.0
 * @Description:    统一返回格式
 */
@Data
public class ResponseData<T> {

    /**
     * 统一返回码
     */
    public Integer code;

    /**
     * 统一消息
     */
    public String msg;

    /**
     * 结果对象
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public T data;

}
