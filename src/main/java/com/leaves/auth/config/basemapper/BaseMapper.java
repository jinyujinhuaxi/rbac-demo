package com.leaves.auth.config.basemapper;


import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 通用基础mapper
 * @param <T>
 *
 * @Author: LEAVES
 * @Date: 2020年10月27日 16时44分28秒
 * @Version 1.0
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {

}
