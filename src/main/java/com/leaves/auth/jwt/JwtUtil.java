package com.leaves.auth.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.leaves.auth.common.utils.IsNotEmptyUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 14时25分08秒
 * @Version 1.0
 * @Description:
 */
@Slf4j
public class JwtUtil {

    //token过期时间     5小时
    private static final long EXPIRE_TIME = 1000 * 60 * 60 * 5;
    //redis中token过期时间   12小时
    public static final Integer REFRESH_EXPIRE_TIME = 60 * 60 * 12;
    //token密钥(自定义)
    private static final String TOKEN_SECRET = "^/zxc*123!@#$%^&*/";


    /**
     * 校验token是否正确
     * @param token token
     * @param username 用户名
     * @return 是否正确
     */
    public static boolean verify(String token, String username){
        log.info("JwtUtil==verify--->");
        try {
            log.info("JwtUtil==verify--->校验token是否正确");
            //根据密码生成JWT效验器
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET); //秘钥是密码则省略
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("username", username)
//                    .withClaim("secret",secret)  //秘钥是密码直接传入
                    .build();
            //效验TOKEN
            DecodedJWT jwt = verifier.verify(token);
            log.info("JwtUtil==verify--->jwt = "+jwt.toString());
            log.info("JwtUtil==verify--->JwtUtil验证token成功!");
            return true;
        }catch (Exception e){
            log.error("JwtUtil==verify--->JwtUtil验证token失败!");
            return false;
        }
    }

    /**
     * 获取token中的信息（包含用户名）
     * @param token
     * @return
     */
    public static String getUsername(String token) {
        log.info("JwtUtil==getUsername--->");
        if (IsNotEmptyUtil.isEmpty(token)){
            log.error("JwtUtil==getUsername--->token无效或token中未包含用户信息！ ");
            throw new AuthenticationException("认证失败，token无效或token中未包含用户信息！");
        }
        try {
            DecodedJWT jwt = JWT.decode(token);
            System.out.println("token = " + jwt.getToken());
            log.info("JwtUtil==getUsername--->username = "+jwt.getClaim("username").asString());
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException e) {
            log.error("JwtUtil==getUsername--->JWTDecodeException: " + e.getMessage());
            return null;
        }
    }

    /**
     * 生成token签名
     * EXPIRE_TIME 分钟后过期
     * @param username 用户名
     * @return 加密的token
     */
    public static String sign(String username) {
        log.info("JwtUtil==sign--->");
        Map<String, Object> header = new HashMap<>();
        header.put("type","Jwt");
        header.put("alg","HS256");
        long currentTimeMillis = System.currentTimeMillis();
        //设置token过期时间
        Date date = new Date(currentTimeMillis + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);//秘钥是密码则省略
        //生成签名
        String sign = JWT.create()
                .withHeader(header)
                .withExpiresAt(date)
                .withClaim("username", username)
//                .withClaim("secret",secret)  //秘钥是密码直接传入
                .withClaim("currentTime", currentTimeMillis + EXPIRE_TIME)
                .sign(algorithm);
        log.info("JwtUtil==sign--->sign = " + sign);
        return sign;
    }

    /**
     * 获取token中用户信息
     * @param token
     * @return
     */
    public static String getAccount(String token){
        try{
            DecodedJWT decodedJWT=JWT.decode(token);
            return decodedJWT.getClaim("username").asString();

        }catch (JWTCreationException e){
            return null;
        }
    }

    /**
     * 获取token中用户密码
     * @param token
     * @return
     */
    public static String getSecret(String token){
        try{
            DecodedJWT decodedJWT=JWT.decode(token);
            return decodedJWT.getClaim("secret").asString();

        }catch (JWTCreationException e){
            return null;
        }
    }

    /**
     * 获取token的时间戳
     * @param token
     * @return
     */
    public static Long getCurrentTime(String token){
        try{
            DecodedJWT decodedJWT=JWT.decode(token);
            return decodedJWT.getClaim("currentTime").asLong();

        }catch (JWTCreationException e){
            return null;
        }
    }
}
