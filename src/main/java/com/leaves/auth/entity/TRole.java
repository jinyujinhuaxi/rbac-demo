package com.leaves.auth.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时06分23秒
 * @Version 1.0
 * @Description: 
 */
/**
    * 角色表
    */
@Data
@AllArgsConstructor
public class TRole {
    /**
    * 角色ID 
    */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
    * 角色编号
    */
    private String name;

    /**
    * 权限ID
    */
    private String auths;

    /**
    * 父级角色ID
    */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentid;
}