package com.leaves.auth.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时06分23秒
 * @Version 1.0
 * @Description: 
 */
/**
    * 用户表
    */
@Data
public class TUser {
    /**
    * 用户ID
    */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
    * 用户名
    */
    private String username;

    /**
    * 用户密码
    */
    private String password;

    /**
    * 用户姓名
    */
    private String name;

    /**
    * 角色ID
    */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roleid;

    /**
    * 年龄
    */
    private Integer age;

    /**
    * 头像
    */
    private String icon;

    /**
    * 性别
    */
    private String sex;

    /**
    * 电话号码
    */
    private String phone;

    /**
    * 邮箱
    */
    private String email;

    /**
    * 所管理的责任区编码
    */
    private String area;

    private TRole role;

}