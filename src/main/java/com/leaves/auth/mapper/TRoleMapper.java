package com.leaves.auth.mapper;

import com.leaves.auth.entity.TRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时06分23秒
 * @Version 1.0
 * @Description: 
 */
@Mapper
@Repository
public interface TRoleMapper {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(TRole record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(TRole record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    TRole selectByPrimaryKey(Long id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(TRole record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TRole record);

    List<String> getPermissionByUserName(@Param("username") String username);

    List<TRole> selectAll();


}