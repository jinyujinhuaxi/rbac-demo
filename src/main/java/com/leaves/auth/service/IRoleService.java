package com.leaves.auth.service;

import com.leaves.auth.response.ResponseData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时20分48秒
 * @Version 1.0
 * @Description:
 */
public interface IRoleService<T> {

    Set<String> getPermissionByUserName(String username);

    ResponseData queryRoleList();
}
