package com.leaves.auth.service;

import com.leaves.auth.entity.TUser;
import com.leaves.auth.response.ResponseData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 13时44分34秒
 * @Version 1.0
 * @Description:
 */
public interface IUserService {

    TUser getUserByUserName(String username);

    ResponseData login(String username, String password, HttpServletResponse response);

    ResponseData logout(HttpServletRequest request);

    ResponseData queryUserList();

    ResponseData queryUserByUserName(String username);

}
