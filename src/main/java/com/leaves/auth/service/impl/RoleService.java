package com.leaves.auth.service.impl;

import com.leaves.auth.entity.TRole;
import com.leaves.auth.mapper.TRoleMapper;
import com.leaves.auth.response.ResponseData;
import com.leaves.auth.response.ResponseDataUtil;
import com.leaves.auth.service.IRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Permissions;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时21分02秒
 * @Version 1.0
 * @Description:
 */
@Service
@Slf4j
public class RoleService implements IRoleService {

    @Resource
    private TRoleMapper roleMapper;

    /**
     * 根据用户查询用户权限（权限认证时调用）
     * @param username
     * @return
     */
    @Override
    public Set<String> getPermissionByUserName(String username) {
        List<String> permissions = roleMapper.getPermissionByUserName(username);
        log.info("权限鉴别===>通过用户名获取用户权限信息：" + permissions.toString());
        return new HashSet<String>(permissions);
    }

    /**
     * 查询全部角色信息
     * @return
     */
    @Override
    public ResponseData queryRoleList() {
        List<TRole> roles = roleMapper.selectAll();
        if (!roles.isEmpty()){
            return ResponseDataUtil.success("查询成功！", roles);
        }
        return ResponseDataUtil.fail("暂无角色数据！");
    }
}
