package com.leaves.auth.exception;

import org.apache.shiro.authc.AuthenticationException;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时06分23秒
 * @Version 1.0
 * @Description:   自定义异常(CustomException)
 */
public class CustomException extends AuthenticationException {

    public CustomException(String msg){
        super(msg);
    }

    public CustomException() {
        super();
    }
}
