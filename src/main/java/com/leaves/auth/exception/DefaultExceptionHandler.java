package com.leaves.auth.exception;


import com.leaves.auth.response.ResponseData;
import com.leaves.auth.response.ResponseDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 11时06分23秒
 * @Version 1.0
 * @Description:   自定义异常处理器
 */
@Slf4j
@ControllerAdvice  //不指定包默认加了@Controller和@RestController都能控制
public class DefaultExceptionHandler {

    /**
     * @param  ex
     * @Description: 运行时异常
     */
    @ExceptionHandler(CustomException.class)
    public ResponseData CustomExceptionHandler(com.leaves.auth.exception.CustomException ex) {
        log.error(ex.getMessage(), ex);
        ResponseData responseData=new ResponseData<>();
        responseData.setCode(400);
        responseData.setMsg(ex.getMessage());
        return responseData;
    }

    /**
     * @param  ex
     * @Description: 权限认证异常
     */
    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public ResponseData unauthorizedExceptionHandler(Exception ex) {
        log.error(ex.getMessage());
        return ResponseDataUtil.fail("对不起，您没有相关权限！");
    }

    /**
     * UnauthenticatedException异常
     * @param ex
     * @return
     */
    @ExceptionHandler(UnauthenticatedException.class)
    @ResponseBody
    public ResponseData unauthenticatedException(UnauthenticatedException ex) {
        log.error(ex.getMessage());
        return ResponseDataUtil.fail("对不起，您未登录！");
    }

    /**
     * 无效token
     * @param ex
     * @return
     */
    @ExceptionHandler(AuthorizationException.class)
    @ResponseBody
    public ResponseData authorizationException(AuthorizationException ex) {
        log.error(ex.getMessage());
        return ResponseDataUtil.fail("无效token，请重新登录！");
    }

    /**
     * 空指针异常
     * @param ex
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public ResponseData nullPointerException(NullPointerException ex) {
        log.error(ex.getMessage(), ex);
        return ResponseDataUtil.fail(500,"空指针异常！");
    }

    /**
     * 认证异常
     * @param ex
     * @return
     */
    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public ResponseData authenticationException(AuthenticationException ex) {
        log.error(ex.getMessage(), ex);
        return ResponseDataUtil.fail("token为空，请重新登录！");
    }

    /**
     * 文件上传异常
     * @param ex
     * @return
     */
    @ExceptionHandler(IOException.class)
    @ResponseBody
    public ResponseData ioException(IOException ex) {
        log.error(ex.getMessage(), ex);
        return ResponseDataUtil.fail("文件上传异常！");
    }

    /**
     * 异常统一自定义处理
     */
    @ExceptionHandler({MyException.class})
    @ResponseBody
    public ResponseData MyException(MyException e) {
        return ResponseDataUtil.fail(500,e.getMessage());
    }

    /**
     * 异常统一处理(最后的异常处理)
     */
    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseData allException(Exception e) {
        log.info(e.getMessage());
        return ResponseDataUtil.fail(500,"系统异常！");
    }
}
