package com.leaves.auth.controller;

import com.leaves.auth.response.ResponseData;
import com.leaves.auth.response.ResponseDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 17时36分46秒
 * @Version 1.0
 * @Description:
 */
@Slf4j
@RestController
@CrossOrigin
public class RedirectController {
    /**
     * 暂无token
     * @return
     */
    @GetMapping("/invalid")
    public Object redirect401(){
        return ResponseDataUtil.fail("token已失效或为空，请先登录！");
    }

    /**
     * 权限不足
     * @return
     */
    @GetMapping("/noaccess")
    public Object redirect403(){
        return ResponseDataUtil.fail("对不起，无权限访问！");
    }

}
