package com.leaves.auth.controller;

import com.leaves.auth.service.IRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 17时59分39秒
 * @Version 1.0
 * @Description:
 */
@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api")
public class RoleController {

    @Resource
    private IRoleService roleService;

    /**
     * 查询全部角色信息
     * @return
     */
    @GetMapping("/role/list")
    public Object queryAll(){
        return roleService.queryRoleList();
    }
}
