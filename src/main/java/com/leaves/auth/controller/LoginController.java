package com.leaves.auth.controller;

import cn.hutool.core.util.StrUtil;
import com.leaves.auth.response.ResponseDataUtil;
import com.leaves.auth.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 18时11分22秒
 * @Version 1.0
 * @Description:
 */
@Slf4j
@RestController
@CrossOrigin
public class LoginController {

    @Resource
    private IUserService userService;

    /**
     * 登录
     * @param username
     * @param password
     * @param response
     * @return
     */
    @PostMapping("/login")
    public Object login(@RequestParam("username")String username, @RequestParam("password")String password, HttpServletResponse response){
        //用户名和密码非空判断
        if (StrUtil.isNotBlank(username) && StrUtil.isNotBlank(password)){
            return userService.login(username, password, response);
        } else {
            log.info("******");
            return ResponseDataUtil.fail("登录失败，用户名或密码为空！");

        }
    }
}
