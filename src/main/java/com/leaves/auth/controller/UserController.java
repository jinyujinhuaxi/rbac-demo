package com.leaves.auth.controller;

import com.leaves.auth.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: LEAVES
 * @Date: 2020年12月30日 17时41分59秒
 * @Version 1.0
 * @Description:
 */
@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api")
public class UserController {

    @Resource
    private IUserService userService;

    /**
     * 退出登录
     * @param request
     * @return
     */
    @GetMapping("/logout")
    public Object logout(HttpServletRequest request){
        return userService.logout(request);
    }

    /**
     * 查询所有用户
     * @return
     */
    @GetMapping("/user/list")
    public Object queryUserList(){
        return userService.queryUserList();
    }

    /**
     * 查询所有用户z
     * @return
     */
    @GetMapping("/user/username")
    @RequiresPermissions(value={"3","1"},logical = Logical.OR)
    public Object queryUserByUserName(@RequestParam("username") String username){
        return userService.queryUserByUserName(username);
    }
}
