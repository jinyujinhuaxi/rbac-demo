/*
 Navicat Premium Data Transfer

 Source Server         : PSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 90619
 Source Host           : localhost:5432
 Source Catalog        : demo
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90619
 File Encoding         : 65001

 Date: 07/01/2021 09:20:05
*/


-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."t_role";
CREATE TABLE "public"."t_role" (
  "id" int8 NOT NULL,
  "name" varchar(64) COLLATE "pg_catalog"."default",
  "auths" varchar(128) COLLATE "pg_catalog"."default",
  "parentid" int8
)
;
COMMENT ON COLUMN "public"."t_role"."id" IS '角色ID ';
COMMENT ON COLUMN "public"."t_role"."name" IS '角色编号';
COMMENT ON COLUMN "public"."t_role"."auths" IS '权限ID';
COMMENT ON COLUMN "public"."t_role"."parentid" IS '父级角色ID';
COMMENT ON TABLE "public"."t_role" IS '角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO "public"."t_role" VALUES (753646510033338311, 'admin', '1', 0);
INSERT INTO "public"."t_role" VALUES (753646510033338362, '客户', '10', 0);
INSERT INTO "public"."t_role" VALUES (753646510034538375, '财务总监', '100', 0);
INSERT INTO "public"."t_role" VALUES (753646510033338321, '人事经理', '2', 0);
INSERT INTO "public"."t_role" VALUES (753646510033338624, '产品总监', '3', 0);
INSERT INTO "public"."t_role" VALUES (753646510033338396, '后端经理', '4', 753646510033338624);
INSERT INTO "public"."t_role" VALUES (753646510033338374, '设计主管', '6', 753646510033338374);
INSERT INTO "public"."t_role" VALUES (753646510033338388, '前端经理', '5', 753646510033338624);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."t_user";
CREATE TABLE "public"."t_user" (
  "id" int8 NOT NULL,
  "username" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(64) COLLATE "pg_catalog"."default",
  "roleid" int8 NOT NULL,
  "age" int4,
  "icon" varchar(128) COLLATE "pg_catalog"."default",
  "sex" varchar(2) COLLATE "pg_catalog"."default",
  "phone" varchar(11) COLLATE "pg_catalog"."default",
  "email" varchar(64) COLLATE "pg_catalog"."default",
  "area" varchar(128) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."t_user"."id" IS '用户ID';
COMMENT ON COLUMN "public"."t_user"."username" IS '用户名';
COMMENT ON COLUMN "public"."t_user"."password" IS '用户密码';
COMMENT ON COLUMN "public"."t_user"."name" IS '用户姓名';
COMMENT ON COLUMN "public"."t_user"."roleid" IS '角色ID';
COMMENT ON COLUMN "public"."t_user"."age" IS '年龄';
COMMENT ON COLUMN "public"."t_user"."icon" IS '头像';
COMMENT ON COLUMN "public"."t_user"."sex" IS '性别';
COMMENT ON COLUMN "public"."t_user"."phone" IS '电话号码';
COMMENT ON COLUMN "public"."t_user"."email" IS '邮箱';
COMMENT ON TABLE "public"."t_user" IS '用户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO "public"."t_user" VALUES (752554210024751106, '100003', 'EFG@AB', '夏雨荷', 753646510033338624, 30, 'https://www.mxxlyj.com/mxhn/1599103997151xlh.jpg', '女', '18888888888', 'clovewb@163.com', '');
INSERT INTO "public"."t_user" VALUES (752554210024751104, '100002', 'EFG@AB', '李思思', 753646510033338396, 23, 'https://www.mxxlyj.com/mxhn/16022223232072b4UeaHS.jpg', '女', '18888888888', 'clovewb@163.com', '');
INSERT INTO "public"."t_user" VALUES (753646510033338313, '100005', 'EFG@AB', '李雨荷', 753646510033338311, 30, 'https://www.mxxlyj.com/mxhn/1602221360306xKGODgrm.jpg', '女', '18888888888', 'clovewb@163.com', '');
INSERT INTO "public"."t_user" VALUES (752554210024751120, '100001', 'EFG@AB', '李白', 753646510033338374, 30, 'https://www.mxxlyj.com/mxhn/1599103997151xlh.jpg', '女', '18888888888', 'clovewb@163.com', '');
INSERT INTO "public"."t_user" VALUES (751042128615833610, '100004', 'EFG@AB', '冷风', 753646510033338321, 19, 'https://www.mxxlyj.com/mxhn/16007544798926pHYOcGZ.jpg', '男', '18888888888', 'clovewb@163.com', '');
INSERT INTO "public"."t_user" VALUES (759778434585985001, 'leaves', 'EFG@AB', '叶子', 753646510034538375, 18, 'https://www.mxxlyj.com/mxhn/1602843544465ujSqZlCN.jpg', '女', '18888888888', 'clovewb@163.com', '');

-- ----------------------------
-- Primary Key structure for table t_role
-- ----------------------------
ALTER TABLE "public"."t_role" ADD CONSTRAINT "T_ROLE_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table t_user
-- ----------------------------
ALTER TABLE "public"."t_user" ADD CONSTRAINT "T_USER_pkey" PRIMARY KEY ("id");
